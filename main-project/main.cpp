#include <iostream>
#include <iomanip>
#include <string.h>

using namespace std;

#include "osadki_opisanie.h"
#include "file_reader.h"
#include "constants.h"

void poisk1(osadki_opisanie* subscriptions[], int size);
void poisk2(osadki_opisanie* subscriptions[], int size);

int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "������������ ������ �8. GIT\n";
    cout << "������� �3. ������\n";
    cout << "�����: ������� ��������\n\n";
    cout << "������ : 16" << endl;
    osadki_opisanie* subscriptions[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", subscriptions, size);
        for (int i = 0; i < size; i++)
        {
            /*******����� ����*******/
            cout << "����.......:" << endl;
            cout << setw(2) << setfill('0') << subscriptions[i]->date.day << '-';
            cout << setw(2) << setfill('0') << subscriptions[i]->date.month << ' ' << endl;
            /*******����� ���������� �������*******/
            cout << setw(4) << subscriptions[i]->kol << " mm" << endl;
            /*******��� �������*******/
            cout << "��� �������....:" << endl;
            cout << subscriptions[i]->title << '\n';
            cout << '\n';
        }
        int a;
        cout << "������� 1 ��� ������ �� �������� �����, 2 ��� ������ �� �������� �� ���������� ������� ������ 10 :" << endl;
        cin >> a;
        cout << "��������� ������ :\n" << endl;
        switch (a) {
        case 1: void poisk1(osadki_opisanie * subscriptions[], int size);
            break;
        case 2: void poisk2(osadki_opisanie * subscriptions[], int size);
            break;
        }

        poisk1(subscriptions, size);
        for (int i = 0; i < size; i++)
        {
            delete subscriptions[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}

void poisk1(osadki_opisanie* subscriptions[], int size) {
    for (int i = 0; i < size; i++) {
        if (strcmp(subscriptions[i]->title, "�����") == 0) {
            /*******����� ����*******/
            cout << "����.......:" << endl;
            cout << setw(2) << setfill('0') << subscriptions[i]->date.day << '-';
            cout << setw(2) << setfill('0') << subscriptions[i]->date.month << ' ' << endl;
            /*******����� ���������� �������*******/
            cout << setw(4) << subscriptions[i]->kol << " mm" << endl;
            /*******��� �������*******/
            cout << "��� �������....:" << endl;
            cout << subscriptions[i]->title << '\n';
            cout << '\n';
        }
    }
}

void poisk2(osadki_opisanie* subscriptions[], int size) {
    for (int i = 0; i < size; i++) {
        if (subscriptions[i]->kol <= 10) {
            /*******����� ����*******/
            cout << "����.......:" << endl;
            cout << setw(2) << setfill('0') << subscriptions[i]->date.day << '-';
            cout << setw(2) << setfill('0') << subscriptions[i]->date.month << ' ' << endl;
            /*******����� ���������� �������*******/
            cout << setw(4) << subscriptions[i]->kol << " mm" << endl;
            /*******��� �������*******/
            cout << "��� �������....:" << endl;
            cout << subscriptions[i]->title << '\n';
            cout << '\n';
        }
    }
}