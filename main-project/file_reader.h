#ifndef FILE_READER_H
#define FILE_READER_H

#include "osadki_opisanie.h"

void read(const char* file_name, osadki_opisanie* array[], int& size);

#endif